/**
 * Created by dkarwot on 30.05.17.
 */
'use strict';

/* global angular */

/* @ngInject */
export default (ScriptTag, $q, $window, BaseLoader) => {
    return class JsApiLoader extends BaseLoader{
        init(settings) {
            settings = settings || {packages: ['corechart']};
            let userCallback = settings.callback || undefined;
            settings.callback = () => {
                if (angular.isFunction(userCallback)) {
                    userCallback.call(this);

                }
                deferred.resolve($window.google);
            };
            const deferred = $q.defer();

            if (this.initCompleted === false) {
                ScriptTag('https://www.google.com/jsapi').then(() => {
                    this.initCompleted = true;
                    this.loadModule('visualization', '1', settings);
                });
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        }

        loadModule(module, version, settings) {
            if (this.initCompleted) {
                $window.google.load(module, version, settings);
            }
        }
    };
};