/**
 * Created by dkarwot on 20.06.17.
 */
/* @ngInject */
export default (ScriptTag, $log, $q, $window, BaseLoader) => {
    return class PlatformLoader extends BaseLoader {
        init() {
            const deferred = $q.defer();
            if (this.initCompleted === false) {
                const g = $window.gapi || ($window.gapi = {});
                g.analytics = {
                    q: []
                };
                g.analytics.ready = (f) => {
                    g.analytics.q.push(f);
                };
                ScriptTag('https://apis.google.com/js/platform.js').then(() => {
                    g.load('analytics');

                    $window.gapi.analytics.ready(() => {
                        this.initCompleted = true;
                        deferred.resolve($window.gapi);
                    });
                });
            } else {
                deferred.resolve($window.gapi);
            }
            return deferred.promise;
        }

        loadModule(module) {
            if (this.initCompleted) {
                $window.gapi.load(module);
            }
        }
    };
};