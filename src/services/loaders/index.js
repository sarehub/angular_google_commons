/**
 * Created by dkarwot on 08.06.17.
 */
'use strict';
/* global angular */

import JsApiLoader from './js-api-loder.factory';
import PlatformLoader from './platform-loader.factory';
import BaseLoader from './base-loader.factory';

export default angular.module('angular-google-commons.services.loaders', [])
    .factory('jsApiLoader', JsApiLoader)
    .factory('GooglePlatformLoader', PlatformLoader)
    .factory('BaseLoader', BaseLoader)
    .name;