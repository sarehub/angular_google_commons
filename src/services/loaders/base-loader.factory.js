/**
 * Created by dkarwot on 18.06.17.
 */

export default () => {
    return class BaseLoader {
        constructor() {
            this.initCompleted = false;
        }

        init() {
        }

        loadModule() {
        }

        isInitCompleted() {
            return this.initCompleted;
        }
    };
};
