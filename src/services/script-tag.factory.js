/**
 * Created by dkarwot on 31.05.17.
 */
'use strict';
/* global angular */

/* @ngInject */
export default function ($q, $document) {

    return (url) => {
        const deferred = $q.defer();

        const tag = angular.element('<script />');

        tag.attr('type', 'text/javascript');
        tag.on('load', () => {
            deferred.resolve();
        });
        tag.on('error', (e) => {
            deferred.reject(e);
        });
        $document.find('head').append(tag);

        tag.attr('src', url);

        return deferred.promise;
    };
}