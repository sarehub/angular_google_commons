/**
 * Created by dkarwot on 08.06.17.
 */
'use strict';

/* global angular */

import Loaders from './loaders';
import ScriptTag from './script-tag.factory';

export default angular.module('angular-google-commons.services', [Loaders])
    .factory('ScriptTag', ScriptTag)
    .name;