/**
 * Created by dkarwot on 08.06.17.
 */
'use strict';
/* global angular */

import Services from './services';

export default angular.module('angular-google-commons', [Services]).name;