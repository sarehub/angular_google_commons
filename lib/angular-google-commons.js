(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("angular-google-commons", [], factory);
	else if(typeof exports === 'object')
		exports["angular-google-commons"] = factory();
	else
		root["angular-google-commons"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by dkarwot on 08.06.17.
 */


/* global angular */

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _loaders = __webpack_require__(3);

var _loaders2 = _interopRequireDefault(_loaders);

var _scriptTag = __webpack_require__(6);

var _scriptTag2 = _interopRequireDefault(_scriptTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = angular.module('angular-google-commons.services', [_loaders2.default]).factory('ScriptTag', _scriptTag2.default).name;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by dkarwot on 08.06.17.
 */

/* global angular */

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _services = __webpack_require__(0);

var _services2 = _interopRequireDefault(_services);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = angular.module('angular-google-commons', [_services2.default]).name;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by dkarwot on 18.06.17.
 */

exports.default = function () {
    return function () {
        function BaseLoader() {
            _classCallCheck(this, BaseLoader);

            this.initCompleted = false;
        }

        _createClass(BaseLoader, [{
            key: "init",
            value: function init() {}
        }, {
            key: "loadModule",
            value: function loadModule() {}
        }, {
            key: "isInitCompleted",
            value: function isInitCompleted() {
                return this.initCompleted;
            }
        }]);

        return BaseLoader;
    }();
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by dkarwot on 08.06.17.
 */

/* global angular */

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _jsApiLoder = __webpack_require__(4);

var _jsApiLoder2 = _interopRequireDefault(_jsApiLoder);

var _platformLoader = __webpack_require__(5);

var _platformLoader2 = _interopRequireDefault(_platformLoader);

var _baseLoader = __webpack_require__(2);

var _baseLoader2 = _interopRequireDefault(_baseLoader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = angular.module('angular-google-commons.services.loaders', []).factory('jsApiLoader', _jsApiLoder2.default).factory('GooglePlatformLoader', _platformLoader2.default).factory('BaseLoader', _baseLoader2.default).name;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by dkarwot on 30.05.17.
 */


/* global angular */

/* @ngInject */

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

exports.default = function (ScriptTag, $q, $window, BaseLoader) {
    return function (_BaseLoader) {
        _inherits(JsApiLoader, _BaseLoader);

        function JsApiLoader() {
            _classCallCheck(this, JsApiLoader);

            return _possibleConstructorReturn(this, (JsApiLoader.__proto__ || Object.getPrototypeOf(JsApiLoader)).apply(this, arguments));
        }

        _createClass(JsApiLoader, [{
            key: 'init',
            value: function init(settings) {
                var _this2 = this;

                settings = settings || { packages: ['corechart'] };
                var userCallback = settings.callback || undefined;
                settings.callback = function () {
                    if (angular.isFunction(userCallback)) {
                        userCallback.call(_this2);
                    }
                    deferred.resolve($window.google);
                };
                var deferred = $q.defer();

                if (this.initCompleted === false) {
                    ScriptTag('https://www.google.com/jsapi').then(function () {
                        _this2.initCompleted = true;
                        _this2.loadModule('visualization', '1', settings);
                    });
                } else {
                    deferred.resolve();
                }

                return deferred.promise;
            }
        }, {
            key: 'loadModule',
            value: function loadModule(module, version, settings) {
                if (this.initCompleted) {
                    $window.google.load(module, version, settings);
                }
            }
        }]);

        return JsApiLoader;
    }(BaseLoader);
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by dkarwot on 20.06.17.
 */
/* @ngInject */
exports.default = function (ScriptTag, $log, $q, $window, BaseLoader) {
    return function (_BaseLoader) {
        _inherits(PlatformLoader, _BaseLoader);

        function PlatformLoader() {
            _classCallCheck(this, PlatformLoader);

            return _possibleConstructorReturn(this, (PlatformLoader.__proto__ || Object.getPrototypeOf(PlatformLoader)).apply(this, arguments));
        }

        _createClass(PlatformLoader, [{
            key: 'init',
            value: function init() {
                var _this2 = this;

                var deferred = $q.defer();
                if (this.initCompleted === false) {
                    var g = $window.gapi || ($window.gapi = {});
                    g.analytics = {
                        q: []
                    };
                    g.analytics.ready = function (f) {
                        g.analytics.q.push(f);
                    };
                    ScriptTag('https://apis.google.com/js/platform.js').then(function () {
                        g.load('analytics');

                        $window.gapi.analytics.ready(function () {
                            _this2.initCompleted = true;
                            deferred.resolve($window.gapi);
                        });
                    });
                } else {
                    deferred.resolve($window.gapi);
                }
                return deferred.promise;
            }
        }, {
            key: 'loadModule',
            value: function loadModule(module) {
                if (this.initCompleted) {
                    $window.gapi.load(module);
                }
            }
        }]);

        return PlatformLoader;
    }(BaseLoader);
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by dkarwot on 31.05.17.
 */

/* global angular */

/* @ngInject */

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function ($q, $document) {

    return function (url) {
        var deferred = $q.defer();

        var tag = angular.element('<script />');

        tag.attr('type', 'text/javascript');
        tag.on('load', function () {
            deferred.resolve();
        });
        tag.on('error', function (e) {
            deferred.reject(e);
        });
        $document.find('head').append(tag);

        tag.attr('src', url);

        return deferred.promise;
    };
};

/***/ })
/******/ ]);
});
//# sourceMappingURL=angular-google-commons.js.map