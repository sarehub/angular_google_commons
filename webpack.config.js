/*global __dirname, require, module*/

const webpack = require('webpack');
const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
const path = require('path');
const AssetsPlugin = require('assets-webpack-plugin');
const env = require('yargs').argv.env; // use --env with webpack 2


let plugins = [new AssetsPlugin()], outputFile;

if (env === 'build') {
    plugins.push(new UglifyJsPlugin({minimize: true}));
    outputFile = '[name]' + '.min.js';
} else {
    outputFile = '[name]' + '.js';
}

const config = {
    entry: {
        'angular-google-commons': __dirname + '/src/module.js'
    },
    devtool: 'source-map',
    output: {
        path: path.resolve(__dirname, 'lib'),
        filename: outputFile,
        library: 'angular-google-commons',
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    module: {
        rules: [
            {
                test: /(\.jsx|\.js)$/,
                loader: 'babel-loader',
                exclude: /(node_modules|bower_components)/,
                options: {
                    plugins: [
                        'transform-es2015-classes',
                        'transform-es2015-arrow-functions',
                        //'transform-es2015-block-scoped-functions',
                        'transform-es2015-block-scoping',
                        'transform-es2015-computed-properties',
                        'transform-es2015-destructuring',
                        'transform-es2015-for-of',
                        'transform-es2015-function-name',
                        'transform-es2015-literals',
                        'transform-es2015-object-super',
                        'transform-es2015-parameters',
                        'transform-es2015-shorthand-properties',
                        'transform-es2015-spread',
                        'transform-es2015-sticky-regex',
                        'transform-es2015-template-literals',
                        'transform-es2015-typeof-symbol',
                        'transform-es2015-unicode-regex',
                        'transform-regenerator'
                    ],
                    presets: [['env', {
                        'targets': {
                            'browsers': ['last 3 versions']
                        }
                    }]]
                }
            },
            {
                test: /(\.jsx|\.js)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        modules: [path.resolve('./src'), 'node_modules'],
        extensions: ['.json', '.js']
    },
    externals: {
        angular: 'angular'
    },
    plugins: plugins
};

module.exports = config;